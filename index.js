console.log('To jest moja pierwsza linia');

max = 10;
var min = 5;
let overMax = 15;
const avg = 7.5;

// avg = 8;

console.log(max, min, overMax, avg);



for(let i = 0; i < 5; i++) {
    console.log(i);
    {
        console.log("\t" + (i*2));
    }
}

console.log('\t');

let num = 13;
let str = 'ciag znakow';

console.log(typeof num);
console.log( typeof(str) );

num = 'trzynascie';
console.log(num, typeof num);

let fT = '13';
let sT = 13;

console.log(typeof fT, typeof sT);
if(fT == sT) {
    console.log('zmienna sa rowne!');
}

console.log(`Witaj numerze ${fT}`);
console.log('Witaj numerze ' + fT);

function x2(x) {
    return x * x;
}

console.log( x2('f2') );

console.log(typeof NaN);

const x3 = function(x) {
    return x * x * x;
}

let line = (x) => {
    console.log(`Przekazales jako argument: ${x}`);
};
console.log( x3(2) );
line('pawel');
console.log(typeof line);
line = 13;

console.log(line, typeof line);

let arr = [1, 2];
console.log(arr);
arr = [5, 6, 7];
console.log(arr);

const newArr = [1, 2];
console.log(newArr);
console.log(newArr instanceof Array);
newArr.push(3);
console.log(newArr);
newArr.shift();
console.log(newArr);
console.log(newArr.indexOf(2));
//newArr = [2, 3];

let obj = {
    x: 13,
    y: 'ciag znakow',
    z: (c) => {
        //console.log('C: ' + c)
        return c.toUpperCase();
    }
};

console.log(obj.x, obj.y, obj.z('pawel'));
let klucz = 'x';
console.log(obj.klucz);
console.log(obj[klucz]);
console.log( Object.keys(obj) );

/*
Napisz funkcje, ktora wypisze typ danych do konsoli przypisany do danego 
klucza obiektu wraz z nazwa klucza np. x - number. Funkcja jako argument
przyjmie obiekt. Dokonaj sprawdzenia czy zostal przekazany prawidlowy typ.

+1. Definicja funkcji
2. Sprawdzenie typu argumentu
3. Wybranie kluczy z obiektu przekazanego jako argument
4. Iteracja po obiekcie dla wybranych kluczy
    > Wypisanie na ekran nazyw klucza i typu jego wartosci
*/
console.log(typeof { x: 13}, typeof [1, 2]);

let showKeysAndTypes = (obj) => {
    if(obj instanceof Object) {
        let keys = Object.keys(obj); // [ 'x', 'y', 'z' ]
        for(let i = 0; i < keys.length; i++) {
            console.log(keys[i], typeof obj[keys[i]]);
        }
    }
}

showKeysAndTypes(obj);
let tempArr = [11, 18, 22];
for(let i in tempArr) {
    console.log(i, tempArr[i]);
}

for(let i of tempArr) {
    console.log(i);
}